#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_i2c.h"
#include "stm32f10x_adc.h"
#include "delay.h"
#include "USART.h"
#include "I2C.h"
#include "ADC.h"
#include "LiquidCrystal_I2C.h"

uint8_t bell[8]  = {0x4,0xe,0xe,0xe,0x1f,0x0,0x4};
uint8_t note[8]  = {0x2,0x3,0x2,0xe,0x1e,0xc,0x0};
uint8_t clock[8] = {0x0,0xe,0x15,0x17,0x11,0xe,0x0};
uint8_t heart[8] = {0x0,0xa,0x1f,0x1f,0xe,0x4,0x0};
uint8_t duck[8]  = {0x0,0xc,0x1d,0xf,0xf,0x6,0x0};
uint8_t check[8] = {0x0,0x1,0x3,0x16,0x1c,0x8,0x0};
uint8_t cross[8] = {0x0,0x1b,0xe,0x4,0xe,0x1b,0x0};
uint8_t retarrow[8] = {0x1,0x1,0x5,0x9,0x1f,0x8,0x4};

uint8_t batt000[8]  = {0x0E,0x1B,0x11,0x11,0x11,0x11,0x11,0x1F};
uint8_t batt010[8]  = {0x0E,0x1B,0x11,0x11,0x11,0x11,0x1F,0x1F};
uint8_t batt030[8]  = {0x0E,0x1B,0x11,0x11,0x11,0x1F,0x1F,0x1F};
uint8_t batt050[8]  = {0x0E,0x1B,0x11,0x11,0x1F,0x1F,0x1F,0x1F};
uint8_t batt070[8]  = {0x0E,0x1B,0x11,0x1F,0x1F,0x1F,0x1F,0x1F};
uint8_t batt090[8]  = {0x0E,0x1B,0x1F,0x1F,0x1F,0x1F,0x1F,0x1F};
uint8_t batt100[8]  = {0x0E,0x1F,0x1F,0x1F,0x1F,0x1F,0x1F,0x1F};
uint8_t battcrg[8]  = {0x0E,0x1F,0x1B,0x17,0x1D,0x1B,0x1F,0x1F};


// Function declarations
// typedef __w64 unsigned int size_t
int strlen(const char *);
char *strrev(char *);
char *itoa(int, char *, int);

int strlen(const char *str) {
	const char *s;

	s = str;
	while (*s)
		s++;
	return s - str;
}

char *strrev(char *str) {
	char *p1, *p2;

	if (!str || !*str)
		return str;

	for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2) {
		*p1 ^= *p2;
		*p2 ^= *p1;
		*p1 ^= *p2;
	}

	return str;
}

char *itoa(int n, char *s, int b) {
	static char digits[] = "0123456789abcdefghijklmnopqrstuvwxyz";
	int i=0, sign;

	if ((sign = n) < 0)
		n = -n;

	do {
		s[i++] = digits[n % b];
	} while ((n /= b) > 0);

	if (sign < 0)
		s[i++] = '-';
	s[i] = '\0';

	return strrev(s);
}


void displayKeyCodes(void) {
  uint8_t i = 0;
  while (1) {
    LCDI2C_clear();
    LCDI2C_setCursor(2,2);
    LCDI2C_write_String("Hello Habrahabr!");
    LCDI2C_setCursor(0, 0);
	char buf[10];
	itoa(i, buf, 10);
    LCDI2C_write_String("Codes 0x"); LCDI2C_write_String(buf);
	itoa(i+19, buf, 10);
    LCDI2C_write_String("-0x"); LCDI2C_write_String(buf);
    LCDI2C_setCursor(0, 1);
    int j;
    for (j=0; j<20; j++) {
      LCDI2C_write(i+j);
    }
    i+=16;
    if (i<15) break;
    Delay(2000);
  }
}

int main()
{
  USART1_Init(); //Вызов функции инициализации периферии

  uint8_t data;
    //дождались команды и начали работу
/*  while(1)
  {
    if((USART1->SR & USART_SR_RXNE)) //Проверяем поступление данных от компьютера
    {
      data = USART1->DR; //Считываем принятые данные
    //  Usart1_Send_symbol(data); //И тут же отсылаем их обратно
      break;
    }
  }*/
 // Delay(3000);
  Usart1_Send_String("Start");

  LCDI2C_init(0x27,16,2);
// ------- Quick 3 blinks of backlight  -------------
//  int i;
//  for( i = 0; i< 3; i++)
//  {
//    LCDI2C_backlight();
//    Delay(250);
//    LCDI2C_noBacklight();
//    Delay(250);
//  }
  LCDI2C_backlight(); // finish with backlight on
  LCDI2C_clear();
//  displayKeyCodes();

//установим кляксу
  LCDI2C_createChar(0, batt000);
  LCDI2C_createChar(1, batt010);
  LCDI2C_createChar(2, batt030);
  LCDI2C_createChar(3, batt050);
  LCDI2C_createChar(4, batt070);
  LCDI2C_createChar(5, batt090);
  LCDI2C_createChar(6, batt100);
  LCDI2C_createChar(7, battcrg);

  int  b=0,i=0,p;
  adc_init();
  uint16_t value = 0;

     while(1){
        LCDI2C_setCursor(0,1);
        char buf[10];
        if ((p>=0) && (p< 100) ) p=i/10;
        else p=100;
        itoa(p, buf, 10);
        LCDI2C_write_String("Charge:");
        LCDI2C_write_String(buf);
        LCDI2C_write(37); //Знак %

        value = get_adc_value();
        LCDI2C_setCursor(0, 0);
        char buf2[10];
        itoa(value, buf2, 10);
        LCDI2C_write_String("ADC: "); LCDI2C_write_String(buf2);

        if (p < 10)  b=0;
        else if (p < 30)  b=1;
        else if (p < 50)  b=2;
        else if (p < 70)  b=3;
        else if (p < 90)  b=4;
        else if (p < 100) b=5;
        else if (p = 100) b=6;

        LCDI2C_setCursor(15,0);
        if ((p>=0) && (p<100)) LCDI2C_write(7);
        else LCDI2C_write(6);
        Delay(500);
        LCDI2C_setCursor(15,0);
        LCDI2C_write(b);
        Delay(500);
        i++;
    }
}
